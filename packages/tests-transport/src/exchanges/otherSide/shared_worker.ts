import {SharedWorkerAction} from './action/shared-worker.action';

if (globalThis.toString() !== '[object Window]') {

  SharedWorkerAction.of()
    .then(actions => actions.run());

}
