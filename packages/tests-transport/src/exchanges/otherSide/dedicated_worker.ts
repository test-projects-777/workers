import {DedicatedWorkerAction} from './action/dedicated-worker.action';

if (globalThis.toString() !== '[object Window]') {

  DedicatedWorkerAction.of()
    .then(actions => actions.run());

}
