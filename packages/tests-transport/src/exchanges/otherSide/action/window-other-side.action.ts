import {ContextSide} from '@dwfe/transport';
import {tap} from '@do-while-for-each/rxjs'
import {SideFactory} from '../../side.factory';

export class WindowOtherSideAction {

  static async of(): Promise<WindowOtherSideAction> {
    const side = await SideFactory.get('otherSide', 'window');
    return new WindowOtherSideAction(side);
  }

  constructor(public side: ContextSide) {
    side.received$.pipe(
      tap(data => {
        console.log(`other-window side process received`, data)
        side.send({...data, time: new Date().toISOString()});
      })
    ).subscribe();
  }

  run() {
    setTimeout(() => {
      this.side.send({from: 'other-window 3000'})
    }, 3000)
  }
}
