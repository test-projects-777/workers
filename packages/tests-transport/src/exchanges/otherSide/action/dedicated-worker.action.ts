import {ContextSide} from '@dwfe/transport';
import {tap} from '@do-while-for-each/rxjs'
import {SideFactory} from '../../side.factory';

export class DedicatedWorkerAction {

  static async of(): Promise<DedicatedWorkerAction> {
    const side = await SideFactory.get('otherSide', 'dedicated_worker');
    return new DedicatedWorkerAction(side);
  }

  constructor(public side: ContextSide) {
    side.received$.pipe(
      tap(data => {
        console.log(`worker process received`, data)
        side.send({...data, time: new Date().toISOString()});
      })
    ).subscribe();
  }

  run() {
    setTimeout(() => {
      this.side.send({from: 'worker 3000'})
    }, 3000)

    setTimeout(() => {
      this.side.send({from: 'worker 5000'})
    }, 5000)
  }
}
