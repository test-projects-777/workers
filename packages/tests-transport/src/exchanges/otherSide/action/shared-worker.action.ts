import {ContextSide} from '@dwfe/transport';
import {tap} from '@do-while-for-each/rxjs'
import {SideFactory} from '../../side.factory';

export class SharedWorkerAction {

  static async of(): Promise<SharedWorkerAction> {
    const side = await SideFactory.get('otherSide', 'shared_worker');
    return new SharedWorkerAction(side);
  }

  constructor(public side: ContextSide) {
    side.received$.pipe(
      tap(data => {
        console.log(`worker process received`, data)

        const {canvas, id} = data

        const ctx = canvas.getContext('2d') as CanvasRenderingContext2D
        ctx.lineWidth = 2
        switch (id) {
          case 'c1':
            ctx.strokeStyle = 'darkblue'
            ctx.beginPath()
            ctx.moveTo(10, 250)
            ctx.lineTo(60, 150)
            ctx.lineTo(120, 350)
            ctx.lineTo(200, 200)
            ctx.lineTo(300, 300)
            ctx.lineTo(380, 30)
            break;
          case 'c2':
            ctx.strokeStyle = 'red'
            ctx.arc(150, 200, 100, 0, 2 * Math.PI)
        }
        ctx.stroke()

        this.side.send('drawn' + new Date().toISOString())
      })
    ).subscribe();
  }

  run() {

  }

}
