import {IConverter} from '@dwfe/transport';

export type TSide = 'main' | 'otherSide';
export type TExchange = 'dedicated_worker' | 'shared_worker' | 'window'

export type TSideData = {
  converter: {
    [key in TSide]: IConverter
  }
}
