import {IConverter, IMessagePost} from '@dwfe/transport';

export class SharedWorkerMainConverter<TSend = any, TPost = any, TRead = any, TReceived = any> implements IConverter<TSend, TPost, TRead, TReceived> {

  write(data: TSend): IMessagePost<TPost> {
    return data as any as IMessagePost<TPost>;
  }

  read(e: MessageEvent<TRead>): TReceived {
    return e.data as any as TReceived; // do nothing
  }

}
