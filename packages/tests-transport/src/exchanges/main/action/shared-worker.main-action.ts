import {ContextSide, IMessagePost} from '@dwfe/transport';
import {tap} from '@do-while-for-each/rxjs';
import {SideFactory} from '../../side.factory';

const canvasId1 = 'c1'
const canvasId2 = 'c2'

function createCanvases() {
  const root = document.getElementById('root') as HTMLDivElement
  root.innerHTML = ''

  const canvas1 = document.createElement('canvas');
  canvas1.id = canvasId1
  canvas1.width = 400
  canvas1.height = 400
  root.appendChild(canvas1)

  const canvas2 = document.createElement('canvas');
  canvas2.id = canvasId2
  canvas2.width = 400
  canvas2.height = 310
  root.appendChild(canvas2)
}

export class SharedWorkerMainAction {

  static async of(): Promise<SharedWorkerMainAction> {
    createCanvases()
    const side = await SideFactory.get('main', 'shared_worker');
    return new SharedWorkerMainAction(side);
  }

  constructor(public side: ContextSide) {
    side.received$.pipe(
      tap(data => {
        console.log(`main process received`, data)
      })
    ).subscribe();
  }

  run() {
    this.sendCanvasToWorker(canvasId1)
    setTimeout(() => {
      this.sendCanvasToWorker(canvasId2)
    }, 5_000)

  }

  sendCanvasToWorker(id: string) {
    const canvas = document.getElementById(id) as HTMLCanvasElement
    const offscreen = canvas.transferControlToOffscreen()
    this.side.send({
      message: {canvas: offscreen, id},
      transfer: [offscreen]
    } as IMessagePost)
  }

}
