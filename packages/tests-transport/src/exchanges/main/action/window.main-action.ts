import {ContextSide} from '@dwfe/transport';
import {tap} from '@do-while-for-each/rxjs';
import {SideFactory} from '../../side.factory';

export class WindowMainAction {

  static async of(): Promise<WindowMainAction> {
    const side = await SideFactory.get('main', 'window');
    return new WindowMainAction(side);
  }

  constructor(public side: ContextSide) {
    side.received$.pipe(
      tap(data => {
        console.log(`main process received`, data)
      })
    ).subscribe();
  }

  run() {

    setTimeout(() => {
      this.side.send({hello: 'world123'})
    }, 5000)

  }

}
