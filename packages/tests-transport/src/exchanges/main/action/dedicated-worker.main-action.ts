import {ContextSide} from '@dwfe/transport';
import {tap} from '@do-while-for-each/rxjs';
import {SideFactory} from '../../side.factory';

export class DedicatedWorkerMainAction {

  static async of(): Promise<DedicatedWorkerMainAction> {
    const side = await SideFactory.get('main', 'dedicated_worker');
    return new DedicatedWorkerMainAction(side);
  }

  constructor(public side: ContextSide) {
    side.received$.pipe(
      tap(data => {
        console.log(`main process received`, data)
      })
    ).subscribe();
  }

  run() {
    this.side.send({hello: 'world123'})

    setTimeout(() => {
      this.side.send({before: 'die'})
      this.side.stop();
    }, 4000)
  }

}
