import {ContextSide, StructuredCloneConverter} from '@dwfe/transport';
import {SharedWorkerMainConverter} from './main/converter/shared-worker.main-converter';
import {TExchange, TSide, TSideData} from './contract';

export const exchanges = new Map<TExchange, TSideData>([
  ['dedicated_worker', {converter: {main: new StructuredCloneConverter(), otherSide: new StructuredCloneConverter()}}],
  ['shared_worker', {converter: {main: new SharedWorkerMainConverter(), otherSide: new StructuredCloneConverter()}}],
  ['window', {converter: {main: new StructuredCloneConverter(), otherSide: new StructuredCloneConverter()}}],
]);

export class SideFactory {

  static async get(side: TSide, exchangeType: TExchange): Promise<ContextSide> {
    if (!exchanges.has(exchangeType))
      throw new Error(`unknown exchange '${exchangeType}'`);

    let ctx: ContextSide;
    const {converter} = exchanges.get(exchangeType) as TSideData;

    switch (side) {
      case 'main':
        if (exchangeType === 'window') {
          const win = openWindow()
          ctx = new ContextSide(side, win, converter.main)
        } else {
          const worker = await buildWorker(exchangeType);
          ctx = new ContextSide(side, worker, converter.main)
        }
        break;
      case 'otherSide':
        if (exchangeType === 'window')
          ctx = new ContextSide(side, window.opener, converter.otherSide)
        else
          ctx = new ContextSide(side, globalThis, converter.otherSide);
        break;
      default:
        throw new Error(`unknown side '${side}'`);
    }
    ctx.setDebug(true);
    return ctx;
  }

}

/**
 * На сервере хранится JSON файл.
 * В нем для каждой entrypoint(это те, которые в конфиге webpack'а указываются)
 * перечислены все приндалежащие ей чанки, например:
 *   {
 *     "bundle": {
 *       "js": [
 *         "vendors~bundle.a050e0555c37fde16319.js",
 *         "bundle.b86fbd84472582bb0312.js"
 *       ]
 *     },
 *     "worker_01": {
 *       "js": [
 *         "worker_01.cf56c35947bd36dc2100.js"
 *       ]
 *     }
 *   }
 */
const buildWorker = (exchangeType: TExchange): Promise<any> => {
  const entrypoint = exchangeType;
  let constructor;
  switch (exchangeType) {
    case 'dedicated_worker':
      constructor = Worker;
      break;
    case 'shared_worker':
      constructor = SharedWorker;
      break;
    default:
      throw new Error(`can't build worker for type "${exchangeType}"`)
  }
  return fetch('./chunk_list_by_entrypoint.json')  // 1) получить JSON файл с информацией обо всех чанках
    .then(response => response.json())             // 2) получить из него список чанков воркера
    .then(json => json[entrypoint].js[0])          // 3) получить название конкретного файла, содержащего код воркера
    .then(fileName => new constructor(`/${fileName}`))  // 4) создать воркер. Файл воркера лежит на сервере!
}

const openWindow = (): Window => window.open(window.origin) as Window
