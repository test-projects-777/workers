import {DedicatedWorkerMainAction} from './src/exchanges/main/action/dedicated-worker.main-action';
import {SharedWorkerMainAction} from './src/exchanges/main/action/shared-worker.main-action';
import {WindowMainAction} from './src/exchanges/main/action/window.main-action';
import {WindowOtherSideAction} from './src/exchanges/otherSide/action/window-other-side.action';

// DedicatedWorkerMainAction.of()
//   .then(actions => actions.run());

// SharedWorkerMainAction.of()
//   .then(actions => actions.run());


if (window.opener) {
  WindowOtherSideAction.of()
    .then(actions => actions.run());
} else {
  WindowMainAction.of()
    .then(actions => actions.run());
}
