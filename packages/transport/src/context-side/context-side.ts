import {map, merge, Observable, share, Stopper, takeUntil, tap} from '@do-while-for-each/rxjs'
import {IStoppable} from '@do-while-for-each/common'
import {Ctx, IConverter, IPostMessageData} from './contract'
import {AbstractContext} from './context/abstract.context'
import {ContextFactory} from './context/context.factory'

export class ContextSide<TSend = any, TPost = any, TRead = any, TReceived = any> implements IStoppable {

  public context: AbstractContext
  private stopper = new Stopper()
  public isDebug = false

  constructor(public readonly id: string,
              ctx: Ctx,
              public readonly converter: IConverter<TSend, TPost, TRead, TReceived>) {
    this.context = ContextFactory.get(ctx);
    merge(
      this.send$,
      this.error$,
    ).pipe(
      takeUntil(this.stopper.ob$)
    ).subscribe();
  }

  send(data: TSend) {
    this.context.send(data)
  }

  get send$(): Observable<IPostMessageData<TPost>> {
    if (!this._send$)
      this._send$ = this.context.send$.pipe(
        takeUntil(this.stopper.ob$),
        tap(d => this.log('to converter.write', d)),
        map(d => this.converter.write(d)),        // TSend -> TPost
        tap(data => this.log('to postMessage', data)),
        tap(data => this.context.postMessage(data)),
        share(),
      )
    return this._send$;
  }

  get received$(): Observable<TReceived> {
    if (!this._received$)
      this._received$ = this.context.received$.pipe(
        takeUntil(this.stopper.ob$),
        tap(event => this.log('to converter.read', event.data)),
        map(event => this.converter.read(event)), // TRead -> TReceived
        tap(data => this.log('to received', data)),
        share(),
      )
    return this._received$;
  }

  get error$() {
    if (!this._error$)
      this._error$ = this.context.error$.pipe(
        takeUntil(this.stopper.ob$),
        tap(event => this.logError('messageerror', event)),
        share(),
      )
    return this._error$;
  }

  stop() {
    this.log('stopping...')
    this.context.stop()
    this.stopper.stop()
  }

  terminate() {
    this.stop()
    this.log('terminating...')
    this.context.terminate()
  }

//region Support

  setDebug(value: boolean): void {
    this.isDebug = value;
  }

  logPrefix = `[${this.id}]:`

  log(...args) {
    if (this.isDebug)
      console.log(this.logPrefix, ...args)
  }

  logError(...args) {
    console.error(this.logPrefix, ...args)
  }


  /**
   * При инициализации класса ContextSide переменные send$, received$ и error$ еще не готовы,
   * но важно, чтобы они возвращали одно и тоже значение, поэтому используется кеш.
   */
  private _send$?: Observable<IPostMessageData<TPost>>;
  private _received$?: Observable<TReceived>;
  private _error$?: Observable<MessageEvent>;

//endregion

}
