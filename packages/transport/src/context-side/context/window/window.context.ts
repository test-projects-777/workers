import {filter, fromEvent, Observable} from '@do-while-for-each/rxjs';
import {AbstractContext} from '../abstract.context';
import {IPostMessageData} from '../../contract';

export class WindowContext<TSend = any, TPost = any, TRead = any> extends AbstractContext<TSend, TPost, TRead> {

  constructor(public ctx: any) { // ctx: Window
    super(ctx)
  }

  async postMessage(data: IPostMessageData<TPost>): Promise<void> {
    if (data.transfer)
      this.ctx.postMessage(data.message, globalThis.origin, data.transfer);
    else
      this.ctx.postMessage(data.message, globalThis.origin)
  }

  received$: Observable<MessageEvent<TRead>> = fromEvent<MessageEvent<TRead>>(globalThis, 'message').pipe(
    filter(event => event.origin === globalThis.origin),
    filter(event => event.data?.['type'] !== 'webpackOk'),
  );

  error$: Observable<MessageEvent> = fromEvent<MessageEvent>(globalThis, 'messageerror');

  terminate(): void {

  }

}
