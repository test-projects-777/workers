import {ServiceWorkerGlobalScopeContext} from './service-worker/service-worker-global-scope.context';
import {SharedWorkerGlobalScopeContext} from './shared-worker/shared-worker-global-scope.context';
import {DedicatedWorkerGlobalScopeContext} from './worker/dedicated-worker-global-scope.context';
import {ServiceWorkerContainerContext} from './service-worker/service-worker-container.context';
import {SharedWorkerContext} from './shared-worker/shared-worker.context';
import {DedicatedWorkerContext} from './worker/dedicated-worker.context';
import {WindowContext} from './window/window.context';
import {AbstractContext} from './abstract.context';
import {Ctx} from '../contract';

export class ContextFactory {

  static get<TSend = any, TPost = any, TRead = any>(ctx: Ctx): AbstractContext<TSend, TPost, TRead> {
    switch (ctx.toString()) {
      case '[object Worker]':
        return new DedicatedWorkerContext(ctx as Worker);
      case '[object DedicatedWorkerGlobalScope]':
        return new DedicatedWorkerGlobalScopeContext(ctx as DedicatedWorkerGlobalScope)
      case '[object ServiceWorkerContainer]':
        return new ServiceWorkerContainerContext(ctx as ServiceWorkerContainer)
      case '[object ServiceWorkerGlobalScope]':
        return new ServiceWorkerGlobalScopeContext(ctx as ServiceWorkerGlobalScope)
      case '[object SharedWorker]':
        return new SharedWorkerContext(ctx as SharedWorker)
      case '[object SharedWorkerGlobalScope]':
        return new SharedWorkerGlobalScopeContext(ctx as SharedWorkerGlobalScope)
      case '[object Window]':
        return new WindowContext(ctx)
      default:
        throw new Error(`Unknown context type "${ctx.toString()}"`);
    }
  }

}
