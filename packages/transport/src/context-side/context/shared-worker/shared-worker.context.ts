import {Observable, Subj} from '@do-while-for-each/rxjs';
import {AbstractContext} from '../abstract.context';
import {IPostMessageData} from '../../contract';

export class SharedWorkerContext<TSend = any, TPost = any, TRead = any> extends AbstractContext<TSend, TPost, TRead> {

  private receivedSubj = new Subj<MessageEvent<TRead>>()
  private errorSubj = new Subj<MessageEvent>()

  constructor(public ctx: SharedWorker) {
    super(ctx)
    this.port.onmessage = (e: MessageEvent<TRead>) => this.receivedSubj.setValue(e)
    this.port.onmessageerror = (e: MessageEvent) => this.errorSubj.setValue(e)
  }

  get port(): MessagePort {
    return this.ctx.port
  }

  async postMessage(data: IPostMessageData<TPost>): Promise<void> {
    if (data.transfer)
      this.port.postMessage(data.message, data.transfer)
    else
      this.port.postMessage(data.message)
  }

  received$: Observable<MessageEvent<TRead>> = this.receivedSubj.value$

  error$: Observable<MessageEvent> = this.errorSubj.value$

  stop(): void {
    super.stop()
    this.port.close()
    this.receivedSubj.stop()
    this.errorSubj.stop()
  }

  terminate(): void {

  }

}
