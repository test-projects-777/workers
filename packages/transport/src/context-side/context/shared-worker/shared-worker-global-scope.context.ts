import {Observable, Subj} from '@do-while-for-each/rxjs';
import {AbstractContext} from '../abstract.context';
import {IPostMessageData} from '../../contract';

export class SharedWorkerGlobalScopeContext<TSend = any, TPost = any, TRead = any> extends AbstractContext<TSend, TPost, TRead> {

  public ports: MessagePort[] = []
  private receivedSubj = new Subj<MessageEvent<TRead>>()
  private errorSubj = new Subj<MessageEvent>()

  constructor(public ctx: SharedWorkerGlobalScope) {
    super(ctx)
    /**
     * Всякий раз после поднятия инстанса SharedWorker в main-thread:
     *  - создается новый канал для связи main- и worker-thread;
     *  - порт на стороне main-thread автоматически стартует;
     *  - вызывается событие onconnect в worker-thread.
     */
    ctx.onconnect = (event: MessageEvent) => {
      const port = event.ports[0]
      port.onmessage = (e: MessageEvent<TRead>) => this.receivedSubj.setValue(e)
      port.onmessageerror = (e: MessageEvent) => this.errorSubj.setValue(e)
      port.start()
      this.ports.push(port)
    }
  }

  async postMessage(data: IPostMessageData<TPost>): Promise<void> {
    this.ports.forEach(port => {
      if (data.transfer)
        port.postMessage(data.message, data.transfer)
      else
        port.postMessage(data.message)
    })
  }

  received$: Observable<MessageEvent<TRead>> = this.receivedSubj.value$

  error$: Observable<MessageEvent> = this.errorSubj.value$

  stop(): void {
    super.stop()
    this.ports.forEach(port => port.close())
    this.receivedSubj.stop()
    this.errorSubj.stop()
  }

  terminate(): void {
    this.ctx.close()
  }

}
