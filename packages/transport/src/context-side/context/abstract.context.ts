import {fromEvent, Observable, Subj} from '@do-while-for-each/rxjs';
import {IStoppable} from '@do-while-for-each/common';
import {Ctx, IPostMessageData} from '../contract';

export abstract class AbstractContext<TSend = any, TPost = any, TRead = any> implements IStoppable {

  private sender = new Subj<TSend>({type: 'noShare'})

  protected constructor(public ctx: Ctx) {
  }

  send(data: TSend): void {
    this.sender.setValue(data)
  }

  send$: Observable<TSend> = this.sender.value$;

  abstract postMessage(data: IPostMessageData<TPost>): Promise<void>;

  received$: Observable<MessageEvent<TRead>> = fromEvent<MessageEvent<TRead>>(this.ctx, 'message');

  error$: Observable<MessageEvent> = fromEvent<MessageEvent>(this.ctx, 'messageerror');

  stop(): void {
    this.sender.stop();
  }

  abstract terminate(): void;

}
