import {AbstractContext} from '../abstract.context';
import {IPostMessageData} from '../../contract';

export class ServiceWorkerGlobalScopeContext<TSend = any, TPost = any, TRead = any> extends AbstractContext<TSend, TPost, TRead> {

  constructor(public ctx: ServiceWorkerGlobalScope) {
    super(ctx)
  }

  async postMessage(data: IPostMessageData<TPost>): Promise<void> {
    const clients = await this.ctx.clients.matchAll({includeUncontrolled: false})
    clients.forEach(client => {
      if (data.transfer)
        client.postMessage(data.message, data.transfer)
      else
        client.postMessage(data.message)
    })
  }

  terminate(): void {

  }

}
