import {AbstractContext} from '../abstract.context';
import {IPostMessageData} from '../../contract';

export class ServiceWorkerContainerContext<TSend = any, TPost = any, TRead = any> extends AbstractContext<TSend, TPost, TRead> {

  constructor(public ctx: ServiceWorkerContainer) {
    super(ctx)
  }

  get controller(): ServiceWorker | null {
    return this.ctx.controller
  }

  async postMessage(data: IPostMessageData<TPost>): Promise<void> {
    if (this.controller) {
      if (data.transfer)
        this.controller.postMessage(data.message, data.transfer)
      else
        this.controller.postMessage(data.message)
    }
  }

  terminate(): void {

  }

}
