import {AbstractContext} from '../abstract.context';
import {IPostMessageData} from '../../contract';

export class DedicatedWorkerGlobalScopeContext<TSend = any, TPost = any, TRead = any> extends AbstractContext<TSend, TPost, TRead> {

  constructor(public ctx: DedicatedWorkerGlobalScope) {
    super(ctx)
  }

  async postMessage(data: IPostMessageData<TPost>): Promise<void> {
    if (data.transfer)
      this.ctx.postMessage(data.message, data.transfer)
    else
      this.ctx.postMessage(data.message)
  }

  terminate(): void {
    this.ctx.close()
  }

}
