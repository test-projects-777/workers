export type Ctx =
  Worker | ServiceWorkerContainer | SharedWorker |                                 // for Main-thread execution context
  DedicatedWorkerGlobalScope | ServiceWorkerGlobalScope | SharedWorkerGlobalScope; // for Worker-thread execution context, usually this is reference inside 'self' variable

export interface IConverter<TSend = any, TPost = any, TRead = any, TReceived = any> {

  write(data: TSend): IPostMessageData<TPost>;

  read(event: MessageEvent<TRead>): TReceived;

}

export interface IPostMessageData<TData = any> {
  message: TData;
  transfer?: Transferable[];
}
